﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeManagement.DataAccessLayer.Models
{
    public class FilePath
    {
        public int ID { get; set; }
        public string FileName { get; set; }

        public string Filepath { get; set; }

        public List<IFormFile> Pictures { get; set; }
    }
}
