﻿using System;
using System.Collections.Generic;
using System.IO;
using EmployeeManagement.DataAccessLayer.Models;
using EmployeeManagement.DataAccessLayer.Repositories;
using EmployeeManagement.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeManagement.Controllers
{
    //[Route("[controller]/[action]")]
    public class HomeController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public HomeController(IEmployeeRepository employeeRepository,
            IWebHostEnvironment webHostEnvironment)
        {
            _employeeRepository = employeeRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        //[Route("~/Home")]
        //[Route("~/")]
        public IActionResult Index()
        {
            var employeeList = _employeeRepository.GetAll();
            return View(employeeList);
        }

        //[Route("{id?}")]
        public IActionResult Details(int? id)
        {
            HomeDetailsViewModel homeDetailsViewModel = new HomeDetailsViewModel()
            {
                Employee = _employeeRepository.GetById(id ?? 1),
                PageTitle = "Employee Details"
            };

            return View(homeDetailsViewModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(EmployeeCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                List<FilePath> uniqueFileName = null;

                if (model.Photos != null && model.Photos.Count > 0)
                {
                    for (int i = 0; i < model.Photos.Count;i++)                  
                    {
                        var uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");

                        uniqueFileName.Add(new FilePath { FileName = model.Photos[i].FileName , 
                        Filepath = Path.Combine(uploadsFolder, model.Photos[i].FileName),
                        Pictures = model.Photos[i]
                        });                                                
                       // model.Photos[i].CopyTo(new FileStream(filePath, FileMode.Create));
                    }                  
                }

                var newEmployee = new Employee
                {
                    Name = model.Name,
                    Email = model.Email,
                    Department = model.Department,
                    BankAccountNumber = model.BankAccountNumber,
                    PhotoPath = uniqueFileName
                };

                var response = _employeeRepository.Add(newEmployee);

                if (response != null && response.Id != 0)
                {
                    return RedirectToAction("Details", new { id = response.Id });
                }
            }

            return View();
        }
    }
}
